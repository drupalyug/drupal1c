<?php

/**
 * 1c exchange main callback
 */
function d1c_main() {
  $d1c = new d1c(FALSE);
  if ($d1c->error) {
    $error = $d1c->error;
    array_unshift($error, 'failure');
    return $error;
  }
  switch ($d1c->requestType()) {
    case 'catalog:checkauth':
      variable_del('d1c_count');
      return array(
        'success',
        $d1c->cookie['name'],
        $d1c->cookie['value']
      );
      break;
    case 'catalog:init':
      module_invoke_all('d1c_import_init');
      return array(
        'zip=no',
        'file_limit=0'
      );
      break;
    case 'catalog:file':
      $result = $d1c->save_stream_file_date('filename');
      return $result;
      break;
    case 'catalog:import':
      return d1c_import($d1c);
      break;
  }
  return 'success';
}
