<?php

/**
 * D1c settings form
 */
function d1c_admin_settings_form() {
  $form = array();
  $form['full_action'] = array(
    '#type' => 'select',
    '#title' => t('Action when importing is full catalog'),
    '#options' => d1c_get_full_import_actions()
  );
  return $form;
}

/**
 * D1c parser settings form
 *
 * @see d1c_admin_parser_settings_form_submit()
 * @see d1c_admin_parser_settings_form_validate()
 */
function d1c_admin_parser_settings_form($form, $form_state) {
  $form = array();
  $form['tree'] = array(
    '#type' => 'fieldset',
    '#title' => t('Import file structure'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE
  );
  $settings = variable_get('d1c_tree', array());
  $form['tree']['create'] = array(
    '#type' => 'checkbox',
    '#title' => t('Create tree item')
  );
  $form['tree']['next'] = array(
    '#type' => 'fieldset',
    '#title' => t('Set structure item'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
    '#states' => array(
      'visible' => array(
        ':input[name="tree[create]"]' => array('checked' => TRUE)
      )
    )
  );

  $form['tree']['next']['parent'] = array(
    '#type' => 'textfield',
    '#title' => t('Parent item tag'),
  );
  $form['tree']['next']['parent_name'] = array(
    '#title' => t('Name of tree item'),
    '#type' => 'machine_name',
    '#required' => FALSE,
    '#machine_name' => array(
      'exists' => 'd1c_check_tag_exists',
      'source' => array('tree', 'next', 'parent'),
    )
  );
  $form['tree']['next']['children'] = array(
    '#type' => 'textfield',
    '#title' => t('Children item tag'),
  );
  $form['tree']['next']['children_name'] = array(
    '#title' => t('Name of tree item'),
    '#type' => 'machine_name',
    '#required' => FALSE,
    '#machine_name' => array(
      'exists' => 'd1c_check_tag_exists',
      'source' => array('tree', 'next', 'children'),
    )
  );
  $form['tree']['next']['entity'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="d1c-parser-entity-wrapper">',
    '#suffix' => '</div>',
    '#tree' => 'TRUE'
  );
  $form['tree']['next']['entity']['type'] = array(
    '#type' => 'select',
    '#title' => t('Entity type'),
    '#options' => d1c_get_entity_types(),
    '#ajax' => array(
      'callback' => 'd1c_admin_parser_ajax',
      'wrapper' => 'd1c-parser-entity-wrapper'
    )
  );
  if (isset($form_state['values']['tree']['next']['entity']['type'])) {
    $form['tree']['next']['entity']['bundle'] = array(
      '#type' => 'select',
      '#title' => t('Entity bundle'),
      '#options' => d1c_get_entity_types('bundle', $form_state['values']['tree']['next']['entity']['type']),
    );
  }
  $form['tree']['next']['id_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Id key'),
    '#default_value' => 'Ид',
    '#size' => 20
  );
  $form['tree']['next']['type'] = array(
    '#type' => 'select',
    '#title' => t('Structure type'),
    '#options' => d1c_get_structure_types(),
  );
  $form['tree']['next']['weight'] = array(
    '#type' => 'weight',
    '#title' => t("Weight of priority"),
    '#delta' => 10
  );
  $form['tree']['next']['file_pattern'] = array(
    '#type' => 'textfield',
    '#title' => t('Pattern for filenames'),
    '#description' => t('Set filenames pattern, where this item defined. You can use regular expression.<br> eg: <strong>^import|^offers</strong> - check files, wich names start as import or offers...'),
    '#required' => TRUE,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100
  );
  $form['exists'] = array(
    '#type' => 'fieldset',
    '#tree' => FALSE,
    '#title' => t('Exists items'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE
  );
  $items = d1c_structure_item_load_multiple();
  foreach ($items as $item) {
    $form['exists'][] = array(
      '#type' => 'fieldset',
      '#title' => $item->id,
      '#tree' => FALSE,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      'item' => d1c_structure_item_view($item)
    );
  }
  return $form;
}

function d1c_admin_parser_ajax($form, $form_sate) {
  return isset($form['tree']['next']['entity']) ? $form['tree']['next']['entity'] : $form['entity'];
}

/**
 * D1c settings form validate callback
 *
 * @see d1c_admin_auth_form()
 * @see d1c_admin_auth_form_submit()
 */
function d1c_admin_parser_settings_form_validate(&$form, &$form_state) {
  $values = $form_state['values'];
  $is_new = $values['tree']['create'];
  if ($is_new) {
    $required_fields = array(
      'parent', 'parent_name',
      'children', 'children_name',
      'id_key', 'type'
    );
    $error = FALSE;
    foreach ($required_fields as $field) {
      if (empty($values['tree']['next'][$field])) {
        $error = TRUE;
        form_set_error('tree][next][' . $field, t('!name field is required.', array('!name' => $form['tree']['next'][$field]['#title'])));
      }
    }
    if (empty($values['tree']['next']['entity']['bundle'])) {
      $error = TRUE;
      form_set_error('tree][next][entity', t('Bundle field is required'));
    }
    if (!$error) {
      $exists_keys = db_select('d1c_structure', 's')
              ->condition('s.id', $values['tree']['next']['parent_name'] . ':' . $values['tree']['next']['children_name'])
              ->fields('s', array('id'))
              ->execute()->fetchField();

      $exists_vals = db_select('d1c_structure', 's')
              ->condition('s.parent', $values['tree']['next']['parent'])
              ->condition('s.children', $values['tree']['next']['children'])
              ->fields('s', array('id'))
              ->execute()->fetchField();

      if ($exists_keys) {
        form_set_error('tree][next][parent', t('Current keys pair alredy exists'));
        form_set_error('tree][next][children', t('Current keys pair alredy exists'));
      }
      if ($exists_vals && false) {
        form_set_error('tree][next][parent', t('Current values pair alredy exists'));
        form_set_error('tree][next][children', t('Current values pair alredy exists'));
      }
    }
  }
}

/**
 * D1c settings form submit callback
 *
 * @see d1c_admin_auth_form()
 * @see d1c_admin_auth_form_validate()
 */
function d1c_admin_parser_settings_form_submit(&$form, &$form_state) {
  form_state_values_clean($form_state);
  $submitted = $form_state['values'];
  d1c_structure_item_save($submitted['tree']['next']);
}

/**
 * D1c administration auth settings form
 *
 * @see d1c_admin_auth_form_submit()
 * @see d1c_admin_auth_form_validate()
 */
function d1c_admin_auth_form($form, $form_state) {
  $form = array();
  $form['username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('d1c_username', ''),
    '#required' => TRUE,
    '#description' => t('Spaces are allowed; punctuation is not allowed except for periods, hyphens, apostrophes, and underscores.'),
  );
  $form['password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#description' => t('The security considerations of the current password is not displayed'),
    '#required' => TRUE
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save')
  );
  return $form;
}

/**
 * D1c settings form validate callback
 *
 * @see d1c_admin_auth_form()
 * @see d1c_admin_auth_form_submit()
 */
function d1c_admin_auth_form_validate(&$form, &$form_state) {

  //Check for valid username inputed
  if ($error = user_validate_name($form_state['values']['username'])) {
    form_set_error('username', $error);
  }
}

/**
 * D1c settings form submit callback
 *
 * @see d1c_admin_auth_form()
 * @see d1c_admin_auth_form_validate()
 */
function d1c_admin_auth_form_submit(&$form, &$form_state) {
  form_state_values_clean($form_state);
  $values = $form_state['values'];

  //Save password has to db
  $password_hash = hash('sha256', $values['password'], false);
  variable_set('d1c_password', $password_hash);

  //Save username to db
  variable_set('d1c_username', $values['username']);

  drupal_set_message(t('Settings has ben saved'));
}

/**
 * Delete structure item confirmation
 */
function d1c_structure_item_delete_confirm($form, $form_state, $item) {
  $form['item'] = array(
    '#type' => 'value',
    '#value' => $item
  );
  return confirm_form($form, t('Are you sure you want to delete %parent > %children?', array('%parent' => $item->parent, '%children' => $item->children)), 'admin/config/services/d1c/parser', t('This action cannot be undone.'), t('Delete'), t('Cancel')
  );
}

function d1c_structure_item_delete_confirm_submit(&$form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $item = $form_state['values']['item'];
    d1c_structure_item_delete($item);
    watchdog('d1c', '%parent > %children item is deleted.', array('%parent' => $item->parent, '%children' => $item->children));
    drupal_set_message(t('%parent > %children item is deleted.', array('%parent' => $item->parent, '%children' => $item->children)));
  }

  $form_state['redirect'] = 'admin/config/services/d1c/parser';
}

function d1c_structure_item_edit_form($form, $form_state, $item) {
  $form['id'] = array('#type' => 'value', '#value' => $item->id);
  $form['parent'] = array(
    '#type' => 'textfield',
    '#title' => t('Parent item tag'),
    '#default_value' => $item->parent
  );
  $form['children'] = array(
    '#type' => 'textfield',
    '#title' => t('Children item tag'),
    '#default_value' => $item->children
  );
  $form['entity'] = array(
    '#type' => 'container',
    '#prefix' => '<div id="d1c-parser-entity-wrapper">',
    '#suffix' => '</div>',
    '#tree' => 'TRUE'
  );
  $form['entity']['type'] = array(
    '#type' => 'select',
    '#title' => t('Entity type'),
    '#options' => d1c_get_entity_types(),
    '#default_value' => $item->entity,
    '#ajax' => array(
      'callback' => 'd1c_admin_parser_ajax',
      'wrapper' => 'd1c-parser-entity-wrapper'
    )
  );
  if (isset($form_state['values']['entity']['type'])) {
    $form['entity']['bundle'] = array(
      '#type' => 'select',
      '#title' => t('Entity bundle'),
      '#default_value' => NULL,
      '#options' => d1c_get_entity_types('bundle', $form_state['values']['entity']['type']),
    );
  }
  else {
    $form['entity']['bundle'] = array(
      '#type' => 'select',
      '#title' => t('Entity bundle'),
      '#default_value' => $item->bundle,
      '#options' => d1c_get_entity_types('bundle', $item->entity),
    );
  }
  $form['id_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Id key'),
    '#default_value' => $item->id_key,
    '#size' => 20
  );
  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Structure type'),
    '#default_value' => $item->type,
    '#options' => d1c_get_structure_types(),
  );
  $form['weight'] = array(
    '#type' => 'weight',
    '#default_value' => $item->weight,
    '#title' => t("Weight of priority"),
    '#delta' => 10
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
    '#submit' => array('d1c_structure_item_edit_form_submit')
  );
  $form['actions']['Cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#weight' => 100,
    '#submit' => array('d1c_structure_item_edit_cancel')
  );

  return $form;
}

function d1c_structure_item_edit_cancel(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/config/services/d1c/parser';
}

function d1c_structure_item_edit_form_submit(&$form, &$form_state) {
  form_state_values_clean($form_state);
  d1c_structure_item_save($form_state['values']);
  drupal_set_message(t('Item %parent > %children (%id) has ben updated', array(
    '%parent' => $form_state['values']['parent'],
    '%children' => $form_state['values']['children'],
    '%id' => $form_state['values']['id']
  )));
  $form_state['redirect'] = 'admin/config/services/d1c/parser';
}

function d1c_structure_item_fields_form($form, $form_state, $item) {
  $defaults = d1c_structure_item_field_load($item->id);

  $instances = field_info_instances($item->entity, $item->bundle);
  unset($instances['d1c_id']);
  $entity = entity_get_info($item->entity);
  $label_field = $entity['entity keys']['label'];
  $form['id'] = array(
    '#type' => 'value',
    '#value' => $item->id
  );
  $form[$label_field] = array(
    '#type' => 'fieldset',
    '#title' => $entity['bundles'][$item->bundle]['label'],
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#tree' => TRUE
  );
  $key_desc = t('Set Key, you can set key "inline" (if it linear) or custom if data generated by conditions');
  $form[$label_field]['field'] = array(
    '#type' => 'textfield',
    '#default_value' => empty($defaults[$label_field]) ? '' : $defaults[$label_field]['field'],
    '#description' => t('This is title(label) field, set field name wich contain in xml'),
    '#title' => t('Field for !field', array('!field' => $entity['bundles'][$item->bundle]['label']))
  );
  $form[$label_field]['key'] = array(
    '#type' => 'select',
    '#description' => $key_desc,
    '#default_value' => empty($defaults[$label_field]) ? 'inline' : $defaults[$label_field]['key'],
    '#options' => array(
      'inline' => t('Inline data'),
      'custom' => t('Custom data')
    ),
    '#title' => t('Key for !field', array('!field' => $entity['bundles'][$item->bundle]['label']))
  );

  foreach ($instances as $instance) {
    $form[$instance['field_name']] = array(
      '#type' => 'fieldset',
      '#title' => $instance['label'],
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#tree' => TRUE
    );
    $form[$instance['field_name']]['field'] = array(
      '#type' => 'textfield',
      '#description' => t('Set field name or parent:children pair(for custom) wich contain in xml'),
      '#title' => t('Field for !field', array('!field' => $instance['label'])),
      '#default_value' => empty($defaults[$instance['field_name']]) ? '' : $defaults[$instance['field_name']]['field'],
    );
    $form[$instance['field_name']]['key'] = array(
      '#type' => 'select',
      '#description' => $key_desc,
      '#default_value' => empty($defaults[$instance['field_name']]) ? 'inline' : $defaults[$instance['field_name']]['key'],
      '#options' => array(
        'inline' => t('Inline data'),
        'custom' => t('Custom data')
      ),
      '#title' => t('Key for !field', array('!field' => $instance['label']))
    );
  }
  $form['entity'] = array(
    '#value' => $item->entity,
    '#type' => 'value'
  );
  $form['bundle'] = array(
    '#value' => $item->bundle,
    '#type' => 'value'
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
    '#submit' => array('d1c_structure_item_field_form_submit')
  );
  $form['actions']['Cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#weight' => 100,
    '#submit' => array('d1c_structure_item_edit_cancel')
  );

  return $form;
}

function d1c_structure_item_field_form_submit(&$form, &$form_state) {
  form_state_values_clean($form_state);
  $values = array_filter($form_state['values'], '_filter_structure_item_field');
  d1c_structure_item_field_save($values);
}

function _filter_structure_item_field($item) {
  if (is_array($item) && empty($item['field'])) {
    return FALSE;
  }
  else {
    return TRUE;
  }
}
