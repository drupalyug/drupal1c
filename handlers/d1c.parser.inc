<?php

class d1cParser {

  public $error = array();
  private $filePath;
  private $parser;
  private $tmp;
  private $id = 'Ид';

  public function __construct($uri) {
    $realpath = drupal_realpath($uri);
    if (!file_exists($realpath)) {
      $text = 'File !filename not exists';
      $this->error[] = t($text, array('!filename' => $uri));
      watchdog('D1C error', $text, array('!filename' => $uri));
      return;
    }
    $this->parser = new XMLReader();
    $this->parser->open($realpath);
  }

  private function isElement() {
    return $this->parser->nodeType == XMLREADER::ELEMENT;
  }

  private function checkLocalName($name) {
    return $this->parser->localName == $name;
  }

  private function getObjectFromParser() {
    $expand = $this->parser->expand();
    $dom = new DomDocument();
    $domNode = $dom->importNode($expand, true);
    $dom->appendChild($domNode);
    return simplexml_import_dom($domNode);
  }

  public function getNode($name) {
    while ($this->parser->read()) {
      switch ($this->parser->nodeType) {
        case (XMLREADER::ELEMENT):
          if ($this->checkLocalName($name)) {
            return $this->getObjectFromParser();
          }
          break;
      }
    }
  }

  public function getChilds($parent, $children, $count = 0, $skip = 0) {
    $returnData = array();
    if (!$count) {
      return array($this->getNode($parent));
    }


    while ($this->parser->read()) {
      if ($this->isElement() && $this->checkLocalName($parent)) {
        break;
      }
    }
    while ($this->parser->read()) {
      if ($this->isElement() && $this->checkLocalName($children)) {
        break;
      }
    }
    //Skip elements
    $hasElement = TRUE;
    if ($skip) {
      for ($i = 0; $i < $skip; $i++) {
        $hasElement = $this->parser->next($children);
      }
    }
    if ($count) {
      for ($i = 0; $i < $count; $i++) {
        if ($hasElement) {
          $returnData[] = $this->getObjectFromParser();
        }
        else {
          break;
        }
        $hasElement = $this->parser->next($children);
      }
    }
    else {
      $returnData[] = $this->getObjectFromParser();
    }
    return $returnData;
  }

  public function setXmlObject($xml) {

  }

  public function objectToArray($obj) {
    if (!is_array($obj) && !is_object($obj)) {
      return $obj;
    }

    $obj = (array) $obj;
    foreach ($obj as $key => $value) {
      $obj[$key] = $this->objectToArray($value);
    }
    return $obj;
  }

}
